<?php

namespace App\Interfaces;

interface IBar extends \SplSubject
{
    public function addSample(string $name, string $genre);
    public function playSample(string $name);
    public function playRandomSample();
}
