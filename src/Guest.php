<?php

namespace App;

use App\Abstracts\AGuest;

class Guest extends AGuest
{
    private $state;

    public function getState(): ?string
    {
        return $this->state;
    }

    private function setState(string $state): self
    {
        $this->state = $state;
        return $this;
    }

    public function dance(): void
    {
        $this->setState('dance');
        echo "{$this->name}: dance\n";
    }

    public function drink(): void
    {
        $this->setState('drink');
        echo "{$this->name}: drink\n";
    }

    public function update(\SplSubject $bar): void
    {
        if (in_array($bar->current_genre, $this->genres, true)) {
            $this->dance();
        } else {
            $this->drink();
        }
    }
}
