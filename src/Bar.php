<?php

namespace App;

use App\Interfaces\IBar;

class Bar implements IBar
{
    public $current_genre;

    private $guests;
    private $samples;

    public function addSample(string $name, string $genre): self
    {
        $this->samples[$name] = $genre;
        return $this;
    }

    public function playSample(string $name): void
    {
        if (array_key_exists($name, $this->samples)) {
            $this->current_genre = $this->samples[$name];
            echo "\nPlay {$this->current_genre}: {$name}\n";
            $this->notify();
        } else {
            throw new \Exception("Sample with that name '{$name}' doesn't exist");
        }
    }

    public function playRandomSample(): void
    {
        $sample = array_rand($this->samples);
        $this->current_genre = $this->samples[$sample];
        echo "\nPlay {$this->current_genre}: {$sample}\n";
        $this->notify();
    }

    public function attach(\SplObserver $guest): void
    {
        $this->guests[] = $guest;
    }

    public function detach(\SplObserver $guest): void
    {
        foreach ($this->guests as $key => $value) {
            if ($value === $guest) {
                unset($this->guests[$key]);
            }
        }
    }

    public function notify(): void
    {
        foreach ($this->guests as $guest) {
            $guest->update($this);
        }
    }
}
