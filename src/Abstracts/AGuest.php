<?php

namespace App\Abstracts;

abstract class AGuest implements \SplObserver
{
    protected $name;
    protected $genres;

    public function __construct(string $name, array $genres)
    {
        $this->name = $name;
        $this->genres = $genres;
    }

    abstract public function drink();
    abstract public function dance();
}
