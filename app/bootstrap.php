<?php

require_once __DIR__ . '/../vendor/autoload.php';

use App\Guest;
use App\Bar;
use Faker\Factory;

const ALLOWED_GENRES = [
    'classic', 'blues', 'folk', 'jazz', 'pop', 'rap', 'rock', 'r&b',
];

$get_genres = function() {
    $num = rand(1, 3);

    // use new instance for each Guest
    $faker = Factory::create();

    for ($i = 0; $i < $num; $i++) {
        $genres[] = $faker->unique()->randomElement(ALLOWED_GENRES);
    }

    return $genres;
};

$bar = new Bar();
$faker = Factory::create();

// guests fill bar
echo "Guests:\n";
for ($i = 0; $i < 10; $i++) {
    $name = $faker->name;
    $genres = $get_genres();
    $guest = new Guest($name, $genres);
    $bar->attach($guest);

    $genres = implode(', ', $genres);
    echo "{$name}: {$genres}\n";
}

// samples fill bar
echo "\nSamples:\n";
for ($i = 0; $i < 10; $i++) {
    $title = $faker->realText(24);
    $genre = $faker->randomElement(ALLOWED_GENRES);
    $bar->addSample($title, $genre);

    echo "{$genre}: {$title}\n";
}
